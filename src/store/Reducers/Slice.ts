import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface PageState {  
  activePage: number;
}

const initialState: PageState = {
  activePage: 0
}

export const PageSlice = createSlice({
  name: 'page',
  initialState,
  reducers: {
      setActivePage: (state, action:PayloadAction<number>) => { state.activePage = action.payload }
  },
  extraReducers: {  
  }
})

export default PageSlice.reducer;