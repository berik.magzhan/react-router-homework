import {combineReducers, configureStore} from "@reduxjs/toolkit";
import PageSlice from './Reducers/Slice';

const rootReducer = combineReducers({
  PageSlice
});

export const setupStore = () => { return configureStore({ reducer: rootReducer }) }

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];